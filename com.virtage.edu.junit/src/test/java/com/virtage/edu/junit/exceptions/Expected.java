package com.virtage.edu.junit.exceptions;

import java.util.ArrayList;

import org.junit.Test;

/**
 * Exception test using {@code @Test(expected=ExceptionType}}.
 * 
 * @author libor
 *
 */
public class Expected {

	@Test(expected=IndexOutOfBoundsException.class)
	public void test() {
		new ArrayList<Object>().get(0);			// exception!!
		
		System.out.println("Will be never reached in case of exception above");
	}

}
