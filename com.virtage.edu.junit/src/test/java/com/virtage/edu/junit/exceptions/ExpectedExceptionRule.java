package com.virtage.edu.junit.exceptions;

import java.util.ArrayList;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Alternative exception test using {@link ExpectedException} rule.
 * 
 * @author libor
 *
 */
public class ExpectedExceptionRule {
	
	@Rule
	public ExpectedException ee = ExpectedException.none();

	@Test
	public void test() {
		// set expected exception
		ee.expect(IndexOutOfBoundsException.class);
		
		// before risky code
		new ArrayList<Object>().get(0); 		// exception!!
		
		System.out.println("Will be never reached in case of exception above");
	}

}
