package com.virtage.edu.junit.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HiddenSuprise {

	public class Calculator {
		public long add(long number1, long number2) {
			return number1 + number2;
		}
	}

	@Test
	public void add() throws Exception {
		Calculator calculator = new Calculator();
		assertEquals(4, calculator.add(1, 3));
	}

}
