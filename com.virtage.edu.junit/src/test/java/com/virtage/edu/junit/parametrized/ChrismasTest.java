package com.virtage.edu.junit.parametrized;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Test whether date is December.
 * 
 * @author libor
 *
 */
@RunWith(Parameterized.class)
public class ChrismasTest {
	
	@Parameters(name="{index}: date {0}")
	public static Collection<Calendar[]> data() {
		
		// Kalendáře
		List<Calendar[]> data = Arrays.asList(new Calendar[][] {
				{ new GregorianCalendar(1989, 11, 29) },	// 11 - Dec
				{ new GregorianCalendar(1984, 10, 7) },		// 10 - Nov
				{ new GregorianCalendar(1918, 9, 28) }		// 9 - Oct
		});
		
		return data;
	}
	
	
	private Calendar cal;
	
	
	public ChrismasTest(Calendar cal) {
		this.cal = cal;
	}

	@Test
	public void isDateDecember() {
		assertTrue("Date is not in December!", cal.get(Calendar.MONTH) == 11);
	}

}
