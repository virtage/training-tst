package com.virtage.edu.junit.rules;

import java.io.File;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class TemporaryFolderRule {
	
	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	@Test
	public void test() throws Exception {
		File randomNamedFile = tmp.newFile();
		File namedFile = tmp.newFile("somefile");
		
		File randomNamedFolder = tmp.newFolder();
		File namedFolder = tmp.newFolder("somefolder");
		
		System.out.format("Random file: %s%nFile: %s%nRandom folder: %s%nFolder:%s",
				randomNamedFile, namedFile, randomNamedFolder, namedFolder);
	}

}
