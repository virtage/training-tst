package com.virtage.edu.junit.tests;

import org.junit.Test;

/**
 * Demonstrates that no test order can be predicated or assumed.
 * Compare results across JVMs or with fellow student to see differences. 
 * 
 * @author libor
 *
 */
public class TestOrder {

	@Test
	public void monday() {
		System.out.println("monday()");
	}
	
	@Test
	public void tuesday() {
		System.out.println("tuesday()");
	}
	
	@Test
	public void wednesday() {
		System.out.println("wednesday()");
	}
	
	@Test
	public void thursday() {
		System.out.println("thursday()");
	}
	
	@Test
	public void friday() {
		System.out.println("friday()");
	}
	
	@Test
	public void saturday() {
		System.out.println("saturday()");
	}
	
	@Test
	public void sunday() {
		System.out.println("sunday()");
	}
	
}
