package com.virtage.edu.junit.tests;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.CoreMatchers.theInstance;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.Ignore;
import org.junit.Test;

public class Asserts {
	
	
	
	
	@Test @Ignore
	public void wouldFail() {
		fail("Intentionally fail");
	}

	@Test
	public void basicAsserts() {
		assertArrayEquals("hello".getBytes(), "hello".getBytes());
		assertArrayEquals(new int[] { 1, 2, 3 }, new int[] { 1, 2, 3 });
		
		assertEquals(11, 10);			// !Object.equals()
		assertEquals("hello", "hello");
		assertEquals("Objects not equals", (Object) null, (Object) null);
		
		assertNotEquals(10, 20);		// !Object.equals()
		assertNotEquals("hello", "HELLO");
		
		assertNull(null);
		
		assertNotNull("hello");
		
		assertTrue("hello" instanceof CharSequence);
		
		assertFalse(10 < 5);
		
		// ==
		assertSame(Runtime.getRuntime(), Runtime.getRuntime());
		assertSame(Integer.valueOf(10), 10);
		
		// assertNotSame("hello", "hello") may not work because JVM re-use
		// first String instance twice
		assertNotSame(new String("hello"), new String("hello"));
	}
	
	@Test
	public void thatExamples() {
		// Nullability - nullValue, notNullValue
		assertThat(null, nullValue());
		assertThat("cat", notNullValue());
		
		// Comparsion -  is, equalsTo; isA, instanceOf; sameInstance, theInstance
		assertThat(10, is(10));						// same as is(equalTo(xy))
		assertThat("cat", equalTo("cat"));
		
		assertThat("cat", isA(String.class));		// same as is(instanceOf(xy))
		assertThat("cat", instanceOf(String.class));
		
		assertThat(Integer.valueOf(10), sameInstance(10));	// same as theInstance()
		assertThat(Runtime.getRuntime(), theInstance(Runtime.getRuntime()));
		
		// Strings
		assertThat("cat", startsWith("c"));
		assertThat("cat", endsWith("t"));
		assertThat("cat", containsString("a"));
		
		// Logical -- allOf, anyOf, not, and, or, both, either
		assertThat("cat", allOf(startsWith("c"), containsString("a")));
		assertThat("cat", anyOf(startsWith("at"), endsWith("at")));
		assertThat("cat", not(both(startsWith("at")).and(endsWith("at"))));
		assertThat("cat", not(both(startsWith("at")).and(endsWith("at"))));
		assertThat("cat", either(containsString("ca")).or(containsString("at")));
		
		// Collections - everyItem (all items), hasItem (at least one item)
		assertThat(Arrays.asList("cat", "hat", "rat"), everyItem(endsWith("at")));
		assertThat(Arrays.asList("cat", "hat", "rat"), hasItem(equalTo("cat")));
	}

}
