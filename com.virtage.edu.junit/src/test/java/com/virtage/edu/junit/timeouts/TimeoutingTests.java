package com.virtage.edu.junit.timeouts;

import org.junit.Test;


/**
 * Setting single test timeout. 
 * 
 * @author libor
 *
 */
public class TimeoutingTests {

	@Test(timeout=2000)
	public void tooLong() {
		while (true);
	}
	
}
