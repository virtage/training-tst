package com.virtage.edu.junit.parametrized;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Test whether number is odd giving individual test case run a name.
 * 
 * @author libor
 */
@RunWith(Parameterized.class)
public class OddNumbersTest {
	
	@Parameters(name="{index}: testing number {0}")
	public static Collection<Integer[]> data() {
		
		// Lichá čísla
		List<Integer[]> oddNumbers = Arrays.asList(new Integer[][] {
				{ 1 },
				{ 3 },
				{ 5 },
				{ 6 },		// není liché!
				{ 7 },
				{ 9 },
		});
		
		return oddNumbers;
	}
	
	
	private int number;
	
	
	public OddNumbersTest(int number) {
		this.number = number;
	}

	@Test
	public void isOdd() {
		// Po dělení lichého čísla dvěma musí vždy zbýt 1
		assertEquals("Number is not odd number.", 1, number % 2);
	}

}
