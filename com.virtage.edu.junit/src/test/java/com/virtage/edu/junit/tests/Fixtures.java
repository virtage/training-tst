package com.virtage.edu.junit.tests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * <p>Demonstrates execution order of methods annotated with {@link BeforeClass},
 * {@link AfterClass}, {@link Before} and {@link After}.</p>
 * 
 * <p>Output will be: 
 * <!-- &#064; is escaped at-character to be not interpreted by Javadoc -->
 * <pre>
 * &#064;BeforeClass
 * &#064;Before
 * dummyTest1()
 * &#064;After
 * &#064;Before
 * dummyTest2()
 * &#064;After
 * &#064;AfterClass
 * </pre></p> 
 * 
 * @author libor
 * 
 */
public class Fixtures {

	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("@BeforeClass");
	}

	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("@AfterClass");
	}

	@Before
	public void setUp() {
		System.out.println("@Before");
	}

	@After
	public void tearDown() {
		System.out.println("@After");
	}

	@Test
	public void dummyTest1() {
		System.out.println("dummyTest1()");
	}
	
	@Test
	public void dummyTest2() {
		System.out.println("dummyTest2()");
	}

}
