package com.virtage.edu.junit.assumptions;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.*;
import static org.junit.Assume.assumeThat;

import java.util.Locale;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Assumptions {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void java28() {
		assumeThat("Java 28 required", System.getProperty("java.version"),
				startsWith("1.28"));
		// rest of test...
	}
	
	@Test
	public void zulu() throws Exception {
		assumeThat("Zulu language required", Locale.getDefault().getLanguage(),
				is("zu"));
		// rest of test...
	}
	
}
