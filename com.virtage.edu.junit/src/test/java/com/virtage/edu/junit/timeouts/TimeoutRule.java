package com.virtage.edu.junit.timeouts;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

/**
 * {@link Timeout} rule applies same timeout to all test methods.
 * 
 * @author libor
 *
 */
public class TimeoutRule {
	
	@Rule
	public Timeout timeout = new Timeout(2000);
	

	@Test
	public void tooLong() {
		while (true);
	}
	
	@Test
	public void alsoTooLong() {
		while (true);
	}
	
}
