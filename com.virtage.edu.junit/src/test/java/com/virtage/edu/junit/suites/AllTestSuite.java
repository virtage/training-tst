package com.virtage.edu.junit.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.virtage.edu.junit.rules.TemporaryFolderRule;
import com.virtage.edu.junit.tests.Asserts;
import com.virtage.edu.junit.tests.Fixtures;
import com.virtage.edu.junit.tests.TestOrder;

/**
 * Group and run many test classes as single batch.
 * 
 * @author libor
 *
 */
@RunWith(Suite.class)
@SuiteClasses({
	Asserts.class,
	Fixtures.class,
	TestOrder.class,
	TemporaryFolderRule.class
})
public class AllTestSuite {

}
