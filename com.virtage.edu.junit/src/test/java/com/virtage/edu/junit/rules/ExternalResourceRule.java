package com.virtage.edu.junit.rules;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;

public class ExternalResourceRule {
	
	private DatabaseMock db = new DatabaseMock();
	
	
	@Rule
	public ExternalResource dbResource = new ExternalResource() {
		@Override
		protected void before() throws Throwable {
			db.connect();
		}
		
		@Override
		protected void after() {
			db.disconnect();
		}
	};
	

	@Test
	public void test1() {
		System.out.println("test1()");
	}
	
	@Test
	public void test2() {
		System.out.println("test2()");
	}
	
	@Test
	public void test3() {
		System.out.println("test3()");
	}
	
	
	private static class DatabaseMock {
		public void connect() {
			System.out.println("Database server connecting");
		}

		public void disconnect() {
			System.out.println("Database server disconnecting");
		}
	}

}
