package com.virtage.edu.junit.exceptions;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;

/**
 * Testing exception using try-catch idiom known from JUnit 3.x age.
 * It's still useful if you want to examine exception (message, cause, ...)
 * 
 * @author libor
 *
 */
public class AsInJUnit3 {

	@Test
	public void test() {
		try {
			new ArrayList<Object>().get(0);
			
		} catch (IndexOutOfBoundsException e) {
			assertThat("Exception message doesn't contain accessed 'index'",
					e.getMessage().toUpperCase(), containsString("INDEX"));
			
			return;
			
		}
		
		// if there exception not thrown
		fail("IndexOutOfBoundsException not thrown");
	}

}
