package com.virtage.edu.junit.parametrized;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

/**
 * Demonstrates alternative way for obtaining test parameter via 
 * field injection.
 * 
 * @author libor
 *
 */
@RunWith(Parameterized.class)
public class ArabicToRomanTest {
	
	@Parameters
	public static List<Object[]> data() {
		List<Object[]> data = Arrays.asList(new Object[][] {
				{ 1, "I" },
				{ 2, "II" },
				{ 3, "III" },
				{ 4, "IIV" },		// wrong!
				{ 5, "V" }
		});
		
		return data;
	}
	
	
	@Parameter(0)
	public int arab;
	
	@Parameter(1)
	public String roman;
	
	
	@Test
	public void testArabToRoman() {
		assertEquals("Incorrect conversion to Roman.", getRoman(arab), roman);
	}
	
	
	/** Super silly number conversion from Arab to Roman. */
	private String getRoman(int number) {
		switch (number) {
			case 1:
				return "I";
			case 2:
				return "II";
			case 3:
				return "III";
			case 4:
				return "IV";
			case 5:
				return "V";
	
			default:
				throw new IllegalArgumentException();
		}
	}

}
