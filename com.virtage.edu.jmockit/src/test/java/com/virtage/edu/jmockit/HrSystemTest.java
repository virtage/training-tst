package com.virtage.edu.jmockit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import mockit.Expectations;
import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.Test;

public class HrSystemTest {
	
	@Mocked Db db;			// Db get mocked
	
	@Test
	public void existPerson() throws Exception {
		new Expectations() {{
			Db.select(anyString); result = 0;
			Db.select(anyString); result = 1;
		}};
		
		assertThat(HrSystem.existPerson("Joe Doe"), is(false));
		assertThat(HrSystem.existPerson("Joe Doe"), is(true));
	}
	
	
	
	@Test
	public void insertExistingPerson(@Mocked HrSystem hrSystem) throws Exception {
		new NonStrictExpectations() {
			{
				HrSystem.existPerson(anyString); result = true;
			}
		};
		
		//assertThat(HrSystem.insertPerson("Joe Doe"), is(not(0)));
		assertThat(HrSystem.insertPerson("Joe Doe"), is(0));
		
		
	}
	
	
	@Test
	public void insertNonExistingPerson(@Mocked HrSystem hrSystem) throws Exception {
		new NonStrictExpectations() {{
			HrSystem.existPerson(anyString); result = false;
		}};
		
		assertThat(HrSystem.insertPerson("Joe Doe"), is(not(0)));
		//assertNotEquals(HrSystem.insertPerson("Joe Doe"), 0);
		//assertEquals(0, HrSystem.insertPerson("Joe Doe"));
		//assertThat(HrSystem.insertPerson("Joe Doe"), is(0));
		
		
	}
	
	
//	@Test
//	public void behavioral() throws Exception {
//		
//		// Record phase
//		HrSystem.existPerson(anyString); result = true;
//		
//		// Replay phase
//		DbServer.connect("blah", "user", "pwd");
//		
//		// Verify phase
//		new Verifications() {{
//			mockedDbServer.isEncodingSupported(anyString);
//		}};
//		
//	}
//	
//	@Test
//	public void stateBased() throws Exception {
//		
//	}

}
