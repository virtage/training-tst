package com.virtage.edu.jmockit;

public class HrSystem {
	
	/**
	 * Tells whether person already exists in the database.
	 */
	public static boolean existPerson(String name) {
		int rows = Db.select(
				"select count(*) from persons where name = '" + name + "'");
		
		if (rows == 0) return false;
		
		return true;
	}
	
	
	/**
	 * Inserts new person.
	 * 
	 * @param name Person name being inserted.
	 * @return New person ID or 0 if person already exists
	 */
	public static int insertPerson(String name) {
		if (existPerson(name)) return 0;
		
		int id = Db.insert("insert into persons (name) values ('" + name + "')");
		
		System.out.println(id);
		
		return id;
	}

}
